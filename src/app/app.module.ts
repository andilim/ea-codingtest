import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CarListComponent } from './car-list/car-list.component';
import { CarListService } from './car-list/car-list.service';
import { LoggerModule, NgxLoggerLevel, NGXLogger } from 'ngx-logger';

@NgModule({
  declarations: [
    AppComponent,
    CarListComponent
  ],
  imports: [
    BrowserModule,
    LoggerModule.forRoot({ level: NgxLoggerLevel.TRACE })
  ],
  providers: [
    CarListService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
