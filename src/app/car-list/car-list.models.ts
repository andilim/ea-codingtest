export interface ICarShow {
  name: string;
  cars: Array<ICar>;
}

export interface ICar {
  make: string;
  model: string;
}

export interface ICarShowFlat {
  name: string;
  make: string;
  model: string;
}
