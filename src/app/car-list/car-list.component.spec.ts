import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarListComponent } from './car-list.component';
import { CarListService } from './car-list.service';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';
import { List } from 'linqts';
import { Observable, Observer } from 'rxjs';
import { ICarShow } from './car-list.models';

describe('CarListComponent', () => {
  let component: CarListComponent;
  let fixture: ComponentFixture<CarListComponent>;
  let carListService: CarListService;
  let carShowList: List<ICarShow>;
  let compiled: HTMLDivElement;
  const carShowMockData: ICarShow[] = [
    {
      name: 'Melbourne Motor Show',
      cars: [
        {
          make: 'Julio Mechannica',
          model: 'Mark 4S'
        },
        {
          make: 'Hondaka',
          model: 'Elisa'
        },
        {
          make: 'Moto Tourismo',
          model: 'Cyclissimo'
        },
        {
          make: 'George Motors',
          model: 'George 15'
        },
        {
          make: 'Moto Tourismo',
          model: 'Delta 4'
        }
      ]
    },
    {
      name: 'Cartopia',
      cars: [
        {
          make: 'Moto Tourismo',
          model: 'Cyclissimo'
        },
        {
          make: 'George Motors',
          model: 'George 15'
        },
        {
          make: 'Hondaka',
          model: 'Ellen'
        },
        {
          make: 'Moto Tourismo',
          model: 'Delta 16'
        },
        {
          make: 'Moto Tourismo',
          model: 'Delta 4'
        },
        {
          make: 'Julio Mechannica',
          model: 'Mark 2'
        }
      ]
    }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        HttpClientModule,
        LoggerModule.forRoot({ level: NgxLoggerLevel.TRACE })
      ],
      declarations: [CarListComponent],
      providers: [
        CarListService
      ]
    })
      .compileComponents()
      .then(() => {
        carListService = TestBed.get(CarListService);
        carShowList = new List(carShowMockData);
      });
  }));

  it('should create component', () => {
    spyOnServiceReturnData();
    recreateComponent();
    expect(component).toBeTruthy();
  });

  it('should create carshow flat list', () => {
    spyOnServiceReturnData();
    recreateComponent();
    expect(component.carShowList.Count()).toBeGreaterThan(0);
    carShowList.ForEach((cs) => {
      new List(cs.cars).ForEach((car) => {
        expect(component.
          carShowList.Where(csf => csf.name === cs.name && car.model === csf.model && car.make === csf.make).Count())
          .toEqual(1);
      });
    });
  });

  it('should show car list', () => {
    spyOnServiceReturnData();
    recreateComponent();

    const carMakerElements = compiled.querySelectorAll('.car-maker');
    expect(carMakerElements.length).toEqual(component.getCarMakers().length);
    component.getCarMakers().forEach((make, index) => {
      expect(carMakerElements[index].textContent.trim()).toContain(make);
      const carElements = carMakerElements[index].parentElement.querySelectorAll('.car-model');
      expect(carElements.length).toEqual(component.getCarModelsByMake(make).length);
      component.getCarModelsByMake(make).forEach((model, makeIndex) => {
        expect(carElements[makeIndex].textContent.trim()).toContain(model);
        const carshowElements = carElements[makeIndex].parentElement.querySelectorAll('.car-show');
        expect(carshowElements.length).toEqual(component.getCarShowByMakeAndModel(make, model).length);
        component.getCarShowByMakeAndModel(make, model).forEach((carShow, csIndex) =>{
          expect(carshowElements[csIndex].textContent.trim()).toContain(carShow);
        })
      });
    });

  });

  it ('should show "No cars found"', () => {
    spyOnServiceReturnEmpty();
    recreateComponent();
    const errorTextElement = compiled.querySelector('.error-text');
    expect(errorTextElement).toBeTruthy();
    expect(errorTextElement.textContent.trim()).toContain('No cars found');
  });

  it ('should show "Invalid return data"', () => {
    spyOnServiceReturnInvalidData();
    recreateComponent();
    const errorTextElement = compiled.querySelector('.error-text');
    expect(errorTextElement).toBeTruthy();
    expect(errorTextElement.textContent.trim()).toContain('Invalid return data');

  });

  function recreateComponent() {
    fixture = TestBed.createComponent(CarListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    compiled = fixture.debugElement.nativeElement;
  }

  function spyOnServiceReturnData() {
    spyOn(carListService, 'getCarShows').and.returnValue(
      Observable.create((observer: Observer<ICarShow[]>) => {
        observer.next(carShowMockData);
        observer.complete();
      })
    );
  }

  function spyOnServiceReturnEmpty() {
    spyOn(carListService, 'getCarShows').and.returnValue(
      Observable.create((observer: Observer<ICarShow[]>) => {
        observer.next([]);
        observer.complete();
      })
    );
  }

  function spyOnServiceReturnInvalidData() {
    spyOn(carListService, 'getCarShows').and.returnValue(
      Observable.create((observer: Observer<string>) => {
        observer.next('Failed Downstream Data');
        observer.complete();
      })
    );
  }
});
