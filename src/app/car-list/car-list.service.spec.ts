import { TestBed, async } from '@angular/core/testing';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { CarListService } from './car-list.service';
import { LoggerModule, NgxLoggerLevel, NGXLogger } from 'ngx-logger';

describe('Car List Service', () => {
  let carListSvc: CarListService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        HttpClientModule,
        LoggerModule.forRoot({ level: NgxLoggerLevel.TRACE })
      ],
      providers: [
        CarListService,
        NGXLogger,
        HttpClient
      ]
    }).compileComponents()
      .then(() => {
        carListSvc = TestBed.get(CarListService);
      });
  }));

  it('should return Car Shows List', (done) => {
    carListSvc.getCarShows().subscribe(result => {
      if (typeof result !== 'string') {
        expect(result.length).toBeGreaterThan(0);
        result.forEach(carShow => {
          expect(carShow.cars.length).toBeGreaterThan(0);
        });
      }
      done();
    });
  });
});
