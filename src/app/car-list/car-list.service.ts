import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { NGXLogger } from 'ngx-logger';
import { ICarShow } from './car-list.models';

@Injectable()
export class CarListService {
  constructor(private httpClient: HttpClient,
    private logger: NGXLogger) {

  }

  getCarShows(): Observable<ICarShow[]> {
    // get list of car shows from api
    const epUrl = environment.apiBaseUrl + '/cars';
    this.logger.trace('[getCarShows] epUrl=' + epUrl);
    return this.httpClient.get(epUrl) as Observable<ICarShow[]>;
  }
}
