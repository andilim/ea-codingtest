import { Component, OnInit } from '@angular/core';
import { List } from 'linqts';
import { NGXLogger } from 'ngx-logger';
import { CarListService } from './car-list.service';
import { ICarShow, ICarShowFlat } from './car-list.models';
import { isError } from 'util';

@Component({
  selector: 'app-car-list',
  templateUrl: './car-list.component.html',
  styleUrls: ['./car-list.component.scss']
})
export class CarListComponent implements OnInit {

  carShowList: List<ICarShowFlat>;
  isError = false;
  errorMessage = '';

  constructor(
    private logger: NGXLogger,
    private carListService: CarListService) {

  }

  ngOnInit() {
    this.logger.trace('[ngOnInit]');
    this.isError = false;
    this.carListService.getCarShows().subscribe((result) => {
      if (typeof result === 'string') {
        this.isError = true;
        this.errorMessage = 'Invalid return data: result = "' + result + '"';
      } else {
        const tmpList = new List<ICarShow>(result);
        if (tmpList.Any()) {
          this.carShowList = tmpList.SelectMany(cs => new List(cs.cars)
            .Select(car => ({ name: cs.name, model: car.model, make: car.make }) as ICarShowFlat));
        } else {
          this.isError = true;
          this.errorMessage = 'No cars found';
        }
      }
    }, (error) => {
      this.isError = true;
      this.errorMessage = JSON.stringify(error);
    });
  }

  getCarMakers(): string[] {
    if (this.carShowList) {
      return this.carShowList.Select(cs => cs.make).OrderBy(x => x).Distinct().ToArray();
    }
    return [];
  }

  getCarModelsByMake(make: string) {
    if (this.carShowList) {
      return this.carShowList.Where(cs => cs.make === make)
        .Select(cs => cs.model).OrderBy(x => x).Distinct().ToArray();
    }
    return [];
  }

  getCarShowByMakeAndModel(make: string, model: string) {
    if (this.carShowList) {
      return this.carShowList.Where(cs => cs.make === make && cs.model === model)
        .Select(cs => cs.name).OrderBy(x => x).Distinct().ToArray();
    }
    return [];

  }

}
